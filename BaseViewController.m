//
//  BaseViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "BaseViewController.h"
#import "CFEHomeViewController.h"
#import "ReportListTableViewController.h"
#import "NewReportTableViewController.h"
#import "CFEMaticosViewController.h"
#import "MoreTableViewController.h"
#import "CFENotificationsTableTableViewController.h"
#import "NewReportTableViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    for (NSString* family in [UIFont familyNames])
//    {
//        NSLog(@"%@", family);
//        
//        for (NSString* name in [UIFont fontNamesForFamilyName: family])
//        {
//            NSLog(@"  %@", name);
//        }
//    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([userDefaults objectForKey:@"showIntro"] == nil) {
        [self showIntroWithCrossDissolve];
    } else {
        [self showTabBar];
    }
    
}


-(UIViewController*) viewControllerWithTabTitle:(NSString*)title image:(UIImage*)image viewController:(UIViewController*)viewController
{

    //UIViewController* viewController = [[view alloc] init];
    viewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:image tag:0];
    return viewController;
}


// Create a custom UIButton and add it to the center of our tab bar
-(void) addCenterButtonWithImage:(UIImage*)buttonImage highlightImage:(UIImage*)highlightImage
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"reportActive"] forState:UIControlStateHighlighted];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0)
        button.center = self.tabBar.center;
    else
    {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0;
        button.center = center;
    }
    button.highlighted = NO;
    
    [button addTarget:self
               action:@selector(newReport:)
     forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
}

- (void)showIntroWithCrossDissolve {
    
    UIView *rootView = self.view;
    
    UIColor *color = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    UIFont *font = [UIFont fontWithName:@"Whitney-Semibold" size:16];
    
    
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = @"CFEmáticos";
    page1.titleFont = font;
    page1.titleColor = color;
    page1.desc = @"Ubica los CFEmáticos de tu alrededor, distinguiendo entre los que puedes hacer pagos y/o trámites.";
    page1.descColor = color;
    page1.bgImage = [UIImage imageNamed:@"bg6"];
    page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"map"]];
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.title = @"Historial";
    page2.titleFont = font;
    page2.titleColor = color;
    page2.descColor = color;
    page2.desc = @"Consulta tu recibo actual y anteriores para llevar tu historial de tu consumo del año en curso.";
    page2.bgImage = [UIImage imageNamed:@"bg7"];
    page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"archive2"]];
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.title = @"Registra tu contrato";
    page3.titleFont = font;
    page3.titleColor = color;
    page3.descColor = color;
    page3.desc = @"Solo escanea el codigo de tu recibo y tu contrato quedara registrado.";
    page3.bgImage = [UIImage imageNamed:@"bg6"];
    page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"scan"]];
    
    EAIntroPage *page4 = [EAIntroPage page];
    page4.title = @"Configura";
    page4.titleFont = font;
    page4.titleColor = color;
    page4.descColor = color;
    page4.desc = @"Personaliza tu cuenta activando los modulos que te brinda la aplicación de acuerdo a tus necesidades.";
    page4.bgImage = [UIImage imageNamed:@"bg7"];
    page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"settings"]];
    
    EAIntroPage *page5 = [EAIntroPage page];
    page5.title = @"Reporta";
    page5.titleFont = font;
    page5.titleColor = color;
    page5.descColor = color;
    page5.desc = @"Crea tu reporte de fallas en 3 sencillos pasos, y recibe actualización de su estado.";
    page5.bgImage = [UIImage imageNamed:@"bg7"];
    page5.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"document"]];
    
    EAIntroView *intro = [[EAIntroView alloc] initWithFrame:rootView.bounds andPages:@[page5, page1, page3, page2, page4]];
    [intro.skipButton setTitleColor:color forState:UIControlStateNormal];
    [intro.skipButton setTitle:NSLocalizedString(@"Saltar", nil) forState:UIControlStateNormal];
    [intro.pageControl setPageIndicatorTintColor:[UIColor colorWithRed:0.651 green:0.651 blue:0.651 alpha:1]];
    [intro.pageControl setCurrentPageIndicatorTintColor:color];
    [intro setDelegate:self];
    
    
    [intro showInView:rootView animateDuration:0.3];
}

- (void)showTabBar {
    UIViewController *reportListViewController = [[ReportListTableViewController alloc] init];
    UINavigationController *reportsNavigationController = [[UINavigationController alloc] initWithRootViewController:reportListViewController];
    
    UIViewController *newReportViewController = [[NewReportTableViewController alloc] init];
    UINavigationController *newReportNavigationController = [[UINavigationController alloc] initWithRootViewController:newReportViewController];
    
    UIViewController *cfematicosViewController = [[CFEMaticosViewController alloc] init];
    UINavigationController *cfematicosNavigationController = [[UINavigationController alloc] initWithRootViewController:cfematicosViewController];
    
    //CFEMoreViewController.h
    UIViewController *moreViewController = [[MoreTableViewController alloc] init];
    UINavigationController *moreNavigationController = [[UINavigationController alloc] initWithRootViewController:moreViewController];
    
    self.viewControllers = [NSArray arrayWithObjects:
                            [self viewControllerWithTabTitle:@"Inicio" image:[UIImage imageNamed:@"home"] viewController:[[CFEHomeViewController alloc] init]],
                            [self viewControllerWithTabTitle:@"Reportes" image:[UIImage imageNamed:@"reports"] viewController:reportsNavigationController],
                            [self viewControllerWithTabTitle:@"Reportar" image:nil  viewController:newReportNavigationController],
                            [self viewControllerWithTabTitle:@"CFEmático" image:[UIImage imageNamed:@"cfematicos"]  viewController:cfematicosNavigationController],
                            [self viewControllerWithTabTitle:@"Más" image:[UIImage imageNamed:@"more"]  viewController:moreNavigationController], nil];
    
    
    [self addCenterButtonWithImage:[UIImage imageNamed:@"report"] highlightImage:nil];
    
}

- (void)introDidFinish:(EAIntroView *)introView {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@YES forKey:@"showIntro"];
    [self showTabBar];
}

- (void)newReport:(id)sender {
    NewReportTableViewController *newReportViewController = [[NewReportTableViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newReportViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}
@end
