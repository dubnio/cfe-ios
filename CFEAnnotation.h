//
//  CFEAnnotation.h
//  CFE
//
//  Created by Ernesto Vargas on 10/9/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CFEAnnotation : NSObject <MKAnnotation>

@property (nonatomic, unsafe_unretained, readonly)
CLLocationCoordinate2D coordinate;

@property (nonatomic, copy) NSString  *title;
@property (nonatomic, copy) NSString  *subtitle;
@property (nonatomic, copy) NSString  *image;
/* unsafe_unretained for the same reason as the coordinate property */
@property (nonatomic, unsafe_unretained) MKPinAnnotationColor  pinColor;

- (instancetype) initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates
                               title:(NSString*)paramTitle
                            subTitle:(NSString*)paramSubTitle
                               image:(NSString*)image;

@end
