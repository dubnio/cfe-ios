//
//  MoreTableViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 10/13/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoreTableViewCell.h"
#import "ProfileViewController.h"
#import "PaymentHistoryViewController.h"
#import "MyReceiptsViewController.h"
#import "CFENotificationsTableTableViewController.h"
#import "AppDelegate.h"
#import "CFELoginViewController.h"
#import "MyContractsViewController.h"

@interface MoreTableViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@end
