//
//  RegisterViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 11/3/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

@synthesize username;
@synthesize password;
@synthesize email;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Registro";
    self.view.backgroundColor = [UIColor colorWithRed:0.980 green:0.980 blue:0.980 alpha:1];
    
    //float height = [[UIScreen mainScreen] bounds].size.height;
    float width = [[UIScreen mainScreen] bounds].size.width;
    
    // Close Button
    UIImage* exImage = [UIImage imageNamed:@"close-button"];
    CGRect frameimg = CGRectMake(0, 0, exImage.size.width, exImage.size.height);
    UIButton *closeButton = [[UIButton alloc] initWithFrame:frameimg];
    [closeButton setBackgroundImage:exImage forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    // Form Background
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 93, width, 150)];
    backgroundView.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    backgroundView.layer.borderColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1].CGColor;
    backgroundView.layer.borderWidth = 0.5;
    [self.view addSubview:backgroundView];
    
    // Username TextField
    username = [[UITextField alloc] initWithFrame:CGRectMake(13, 110, width-26, 17.5)];
    username.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    username.font = [UIFont fontWithName:@"Whitney-Medium" size:17.5];
    [username setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    //username.borderStyle = UITextBorderStyleRoundedRect;
    username.placeholder = @"Tu usuario";
    username.autocorrectionType = UITextAutocorrectionTypeNo;
    username.keyboardType = UIKeyboardTypeDefault;
    username.returnKeyType = UIReturnKeyNext;
    username.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [username becomeFirstResponder];
    username.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.view addSubview:username];
    
    UIView *editBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 143, width, 0.5)];
    editBorder.backgroundColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1];
    [self.view addSubview:editBorder];
    
    // Email TextField
    email = [[UITextField alloc] initWithFrame:CGRectMake(13, 160, width-26, 17.5)];
    email.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    email.font = [UIFont fontWithName:@"Whitney-Medium" size:17.5];
    [email setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    email.placeholder = @"Tu correo electronico";
    email.autocorrectionType = UITextAutocorrectionTypeNo;
    email.keyboardType = UIKeyboardTypeEmailAddress;
    email.returnKeyType = UIReturnKeyNext;
    email.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [email becomeFirstResponder];
    email.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.view addSubview:email];
    
    UIView *secondBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 193, width, 0.5)];
    secondBorder.backgroundColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1];
    [self.view addSubview:secondBorder];
    
    // Password TextField
    password = [[UITextField alloc] initWithFrame:CGRectMake(13, 210, width-26, 17.5)];
    password.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    password.font = [UIFont fontWithName:@"Whitney-Medium" size:17.5];
    [password setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    password.placeholder = @"Tu contraseña";
    password.secureTextEntry = YES;
    password.autocorrectionType = UITextAutocorrectionTypeNo;
    password.keyboardType = UIKeyboardTypeDefault;
    password.returnKeyType = UIReturnKeyDone;
    password.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [password becomeFirstResponder];
    password.clearButtonMode = UITextFieldViewModeWhileEditing;
    [password addTarget:self action:@selector(textFieldFinished:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.view addSubview:password];
}

- (void)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)getToken {
    [SVProgressHUD showWithStatus:@"Obteniendo token"];
    RKObjectManager *manager = [RKObjectManager sharedManager];
    
    User *userToken = [User new];
    userToken.username = self.username.text;
    userToken.password = self.password.text;
    
    [manager postObject:userToken path:@"tokenauth/" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        NSLog(@"======> tokenauth/: %@", [result firstObject] );
        [SVProgressHUD showSuccessWithStatus:@"Exitoso"];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[[result firstObject] token] forKey:@"token"];
        [[RKObjectManager sharedManager].HTTPClient setDefaultHeader:@"Authorization" value:[NSString stringWithFormat:@" token %@", [userDefaults objectForKey:@"token"]]];
        
        UIViewController *viewController = [[BaseViewController alloc] init];
        [self.parentViewController.view removeFromSuperview];
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        del.window.rootViewController = viewController;
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"<=====> Error: %@", [[error userInfo] objectForKey:@"NSLocalizedRecoverySuggestion"]);
        [SVProgressHUD showErrorWithStatus:@"Error al obtener token"];
    }];
    
}

- (void)textFieldFinished:(id)sender
{
    
    [SVProgressHUD showWithStatus:@"Registrando"];

    User *user = [User new];
    user.username = self.username.text;
    user.password = self.password.text;
    user.email = self.email.text;
                                
    // POST to create new user
    RKObjectManager *manager = [RKObjectManager sharedManager];

    [manager postObject:user path:@"users/" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSLog(@"======> users/: %@", [result firstObject]);
        [SVProgressHUD showSuccessWithStatus:@"Registro exitoso"];
        [self getToken];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"<===== Error: %@", [[error userInfo] objectForKey:@"NSLocalizedRecoverySuggestion"]);
        [SVProgressHUD showErrorWithStatus:@"Error al registrar"];
    }];


}

@end
