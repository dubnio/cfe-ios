//
//  Contract.h
//  CFE
//
//  Created by Ernesto Vargas on 11/11/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contract : NSObject
    @property (nonatomic, copy) NSString *id;
    @property (nonatomic, copy) NSString *name;
    @property (nonatomic, copy) NSString *user;
    @property (nonatomic, copy) NSString *number;
@end
