//
//  CFENotificationsTableTableViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "CFENotificationsTableTableViewController.h"

@interface CFENotificationsTableTableViewController ()
@property (assign, nonatomic) SystemSoundID bloomSound;
@property (assign, nonatomic) SystemSoundID foldSound;
@property (assign, nonatomic) SystemSoundID selectedSound;
@end

@implementation CFENotificationsTableTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureSounds];
    AudioServicesPlaySystemSound(self.bloomSound);
    self.title = @"Notificaciones";
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Notification button
    UIImage* bellImage = [UIImage imageNamed:@"close-button"];
    CGRect frameimg = CGRectMake(0, 0, bellImage.size.width, bellImage.size.height);
    UIButton *bellButton = [[UIButton alloc] initWithFrame:frameimg];
    [bellButton setBackgroundImage:bellImage forState:UIControlStateNormal];
    [bellButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    //[someButton setShowsTouchWhenHighlighted:YES];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bellButton];
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configureSounds
{
    // Configure bloom sound
    //
    NSString *bloomSoundPath = [[NSBundle mainBundle]pathForResource:@"bloom" ofType:@"caf"];
    NSURL *bloomSoundURL = [NSURL fileURLWithPath:bloomSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)bloomSoundURL, &_bloomSound);
    
    // Configure fold sound
    //
    NSString *foldSoundPath = [[NSBundle mainBundle]pathForResource:@"fold" ofType:@"caf"];
    NSURL *foldSoundURL = [NSURL fileURLWithPath:foldSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)foldSoundURL, &_foldSound);
    
    // Configure selected sound
    //
    NSString *selectedSoundPath = [[NSBundle mainBundle]pathForResource:@"selected" ofType:@"caf"];
    NSURL *selectedSoundURL = [NSURL fileURLWithPath:selectedSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)selectedSoundURL, &_selectedSound);
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

#pragma mark - Actions

- (void)cancel:(id)sender {
    AudioServicesPlaySystemSound(self.foldSound);
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
