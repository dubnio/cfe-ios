//
//  AddContractViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 11/10/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "Contract.h"

@interface AddContractViewController : UIViewController

@property (nonatomic, strong) UITextField *user;
@property (nonatomic, strong) UITextField *name;
@property (nonatomic, strong) UITextField *number;

@end
