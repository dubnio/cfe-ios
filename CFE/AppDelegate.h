//
//  AppDelegate.h
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

