//
//  MyContractsViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 11/3/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddContractViewController.h"
#import "MyContractsTableViewCell.h"
#import "Contract.h"

@interface MyContractsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
