//
//  MyContractsTableViewCell.h
//  CFE
//
//  Created by Ernesto Vargas on 11/11/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyContractsTableViewCell : UITableViewCell
    @property (nonatomic, strong) UILabel *titleLabel;
    @property (nonatomic, strong) UILabel *numberLabel;
    @property (nonatomic, strong) UIImageView *iconImageView;
@end
