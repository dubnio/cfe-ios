//
//  Report.h
//  CFE
//
//  Created by Ernesto Vargas on 11/12/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Report : NSObject
    @property (nonatomic, copy) NSString *contract;
    @property (nonatomic, copy) NSString *category;
    @property (nonatomic, copy) NSString *comments;
    @property (nonatomic, copy) NSString *status;
    @property (nonatomic, copy) NSString *latitude;
    @property (nonatomic, copy) NSString *longitude;
@end
