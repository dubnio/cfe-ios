//
//  main.m
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
