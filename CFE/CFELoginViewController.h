//
//  CFELoginViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"

@interface CFELoginViewController : UIViewController

@property (nonatomic, strong) UITextField *username;
@property (nonatomic, strong) UITextField *password;

@end
