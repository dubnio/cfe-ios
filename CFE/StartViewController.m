//
//  StartViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 11/2/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "StartViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    float height = [[UIScreen mainScreen] bounds].size.height;
    float width = [[UIScreen mainScreen] bounds].size.width;
    float buttonHeight = 60;
    
    // Background Image
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    imgView.image = [UIImage imageNamed:@"startBackground"];
    imgView.contentMode = UIViewContentModeCenter;
    [self.view addSubview:imgView];
    
    // Login Button
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [loginButton setTitle:@"Iniciar Sesión" forState:UIControlStateNormal];
    [loginButton sizeToFit];
    loginButton.frame=CGRectMake(0, height-buttonHeight, width/2, buttonHeight);
    //button.center = CGPointMake(0, 50, 160, 45);
    loginButton.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    //loginButton.layer.borderColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1].CGColor;
    //loginButton.layer.borderWidth = 0.5;
    //loginButton.layer.cornerRadius = 3;
    loginButton.titleLabel.font  = [UIFont fontWithName:@"Whitney-Medium" size:17.5];
    [loginButton setTitleColor:[UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1] forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginButton];
    
    // Register Button
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [registerButton setTitle:@"Registrar" forState:UIControlStateNormal];
    [registerButton sizeToFit];
    registerButton.frame=CGRectMake(width/2, height-buttonHeight, width/2, buttonHeight);
    registerButton.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    registerButton.titleLabel.font  = [UIFont fontWithName:@"Whitney-Medium" size:17.5];
    [registerButton setTitleColor:[UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1] forState:UIControlStateNormal];
    [registerButton addTarget:self action:@selector(regist:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerButton];
 
    // Borders
    UIView *topBorder = [[UIView alloc] initWithFrame:CGRectMake(0, height-buttonHeight, width, 0.5)];
    topBorder.backgroundColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1];
    [self.view addSubview:topBorder];
    UIView *middleBorder = [[UIView alloc] initWithFrame:CGRectMake(width/2, height-buttonHeight, 0.5, buttonHeight)];
    middleBorder.backgroundColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1];
    [self.view addSubview:middleBorder];
}


- (void)login:(id)sender {
    CFELoginViewController *loginViewController = [[CFELoginViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)regist:(id)sender {
    RegisterViewController *loginViewController = [[RegisterViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginViewController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

@end
