//
//  MyContractsViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 11/3/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "MyContractsViewController.h"

@interface MyContractsViewController ()

@end

@implementation MyContractsViewController {
    UITableView *tableView;
    UIRefreshControl *refreshControl;
    NSArray *tableData;
    float width;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Mis Contratos";
    self.view.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButtonItem];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"ReloadMyContractsTable" object:nil];

    width = [[UIScreen mainScreen] bounds].size.width;
    //
    // Add add button
    //
    UIImage* bellImage = [UIImage imageNamed:@"add-button"];
    CGRect frameimg = CGRectMake(0, 0, bellImage.size.width, bellImage.size.height);
    UIButton *bellButton = [[UIButton alloc] initWithFrame:frameimg];
    [bellButton setBackgroundImage:bellImage forState:UIControlStateNormal];
    [bellButton addTarget:self action:@selector(addContract:) forControlEvents:UIControlEventTouchUpInside];
    //[someButton setShowsTouchWhenHighlighted:YES];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bellButton];
    
    //
    // Create TableView
    //
    tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [tableView registerClass:[MyContractsTableViewCell class] forCellReuseIdentifier:@"cell"];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.opaque = NO;
    //tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = tableView;
    
     [self.view addSubview:tableView];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    //[tableView addSubview:refreshControl];
    tableViewController.refreshControl = refreshControl;
    
    [self getContracts];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [tableData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Create Headerview
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 50)];
    tableHeaderView.backgroundColor = [UIColor colorWithRed:0.980 green:0.980 blue:0.980 alpha:1];
    
    // Create Label
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 8, width-10, 20)];
    label.text = @"CONTRATOS";
    label.textColor = [UIColor colorWithRed:0.631 green:0.643 blue:0.651 alpha:1];
    label.font = [UIFont fontWithName:@"Whitney-Semibold" size:12];
    
    [tableHeaderView addSubview:label];
    return tableHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyContractsTableViewCell *cell = (MyContractsTableViewCell*)[theTableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.iconImageView.image = [UIImage imageNamed:@"contract"];
    cell.numberLabel.text = [NSString stringWithFormat:@"# %@", [[tableData objectAtIndex:indexPath.row] number]];
    cell.titleLabel.text = [[tableData objectAtIndex:indexPath.row] name];
    return cell;
}


#pragma mark - Actions

- (void)getContracts {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Contract class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"name":   @"name",
                                                  @"number":     @"number",
                                                  }];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPOST pathPattern:nil keyPath:@"results" statusCodes:nil];
    NSURL *url = [NSURL URLWithString:@"http://cfe.herokuapp.com/api/contracts/"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [mutableRequest addValue:[NSString stringWithFormat:@" token %@", [userDefaults objectForKey:@"token"]] forHTTPHeaderField:@"Authorization"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    

    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSLog(@"Contracts: %@", [result array]);
        tableData = [result array];
        [tableView reloadData];
        
    } failure:nil];
    [operation start];
}

- (void)addContract:(id)sender {
    AddContractViewController *viewController = [[AddContractViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

-(void)refresh {
    [self getContracts];
    [refreshControl endRefreshing];
}

@end
