//
//  NRContractsViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 11/12/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyContractsTableViewCell.h"
#import "Contract.h"
#import "AppDelegate.h"
#import "CreateNewReportViewController.h"

@interface NRContractsViewController : UIViewController  <UITableViewDelegate, UITableViewDataSource>
    @property (nonatomic, copy) NSString *reportTypeId;
@end
