//
//  ReportListTableViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/12/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "ReportListTableViewController.h"

@interface ReportListTableViewController ()
    @property (nonatomic, strong) NSMutableArray           *sectionTitles;
@end

@implementation ReportListTableViewController {
    UITableView *tableView;
    UIRefreshControl *refreshControl;
    NSArray *tableData;
    UIView *emptyMessage;
    float width;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Reportes";
    // self.clearsSelectionOnViewWillAppear = NO;
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    //float height = [[UIScreen mainScreen] bounds].size.height;
    width = [[UIScreen mainScreen] bounds].size.width;
    emptyMessage = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 75)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"ReloadReportListTable" object:nil];
    
    tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [tableView registerClass:[ReportListTableViewCell class] forCellReuseIdentifier:@"cell"];

    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.opaque = NO;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = tableView;
    
    [self.view addSubview:tableView];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    //[refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventEditingDidBegin];
    //refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Tirar para refrescar"];
    //[tableView addSubview:refreshControl];
    tableViewController.refreshControl = refreshControl;
    

    
    self.sectionTitles = [[NSMutableArray alloc] initWithObjects:@"REPORTES ABIERTOS", nil];
    
    // Notification button
    UIImage* bellImage = [UIImage imageNamed:@"bell"];
    CGRect frameimg = CGRectMake(0, 0, bellImage.size.width, bellImage.size.height);
    UIButton *bellButton = [[UIButton alloc] initWithFrame:frameimg];
    [bellButton setBackgroundImage:bellImage forState:UIControlStateNormal];
    [bellButton addTarget:self action:@selector(toggleNotifications:) forControlEvents:UIControlEventTouchUpInside];
    //[someButton setShowsTouchWhenHighlighted:YES];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bellButton];
    
    
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 184, width, 50)];
    imgView.image = [UIImage imageNamed:@"empty"];
    imgView.contentMode = UIViewContentModeCenter;
    //[self.view addSubview:imgView];
    [emptyMessage addSubview:imgView];
    
    
    UILabel *emtyLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 266, width, 17)];
    emtyLabel.textColor = [UIColor colorWithRed:0.247 green:0.247 blue:0.247 alpha:1];
    emtyLabel.font = [UIFont fontWithName:@"Whitney-Semibold" size:16];
    emtyLabel.text = @"No hay reportes abiertos";
    emtyLabel.textAlignment = NSTextAlignmentCenter;
    //[self.view addSubview:emtyLabel];
    
    [emptyMessage addSubview:emtyLabel];
    emptyMessage.alpha = 0;
    
    [self.view addSubview:emptyMessage];
    
    // table data is empty ?
    if (![tableData count]) {
        [UIView animateWithDuration:0.5 animations:^{ emptyMessage.alpha = 0; }];
    } else {
        [UIView animateWithDuration:0.5 animations:^{ emptyMessage.alpha = 1; }];
    }
    
    [self getReports];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [tableData count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 108;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Create Headerview
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 30)];
    tableHeaderView.backgroundColor = [UIColor colorWithRed:0.980 green:0.980 blue:0.980 alpha:1];
    
    // Create Label
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 8, width, 50)];
    label.text = [self.sectionTitles objectAtIndex:section];
    label.textColor = [UIColor colorWithRed:0.631 green:0.643 blue:0.651 alpha:1];
    label.font = [UIFont fontWithName:@"Whitney-Semibold" size:12];
    
    [tableHeaderView addSubview:label];
    return tableHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ReportListTableViewCell *cell = (ReportListTableViewCell*)[theTableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.titleLabel.text = [[tableData objectAtIndex:indexPath.row] comments];
    cell.statusTypeLabel.text = [[tableData objectAtIndex:indexPath.row] status];
    cell.addressLabel.text = @"Nuevo León, Col Hipódromo Condesa";
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - Actions

- (void)getReports {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Report class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"contract": @"contract",
                                                  @"category": @"category",
                                                  @"comments": @"comments",
                                                  @"status": @"status"
                                                  }];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPOST pathPattern:nil keyPath:@"results" statusCodes:nil];
    NSURL *url = [NSURL URLWithString:@"http://cfe.herokuapp.com/api/reports/"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [mutableRequest addValue:[NSString stringWithFormat:@" token %@", [userDefaults objectForKey:@"token"]] forHTTPHeaderField:@"Authorization"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSLog(@"Reports: %@", [result array]);
        tableData = [result array];
        
        // table data is empty ?
        if (![[result array] count]) {
            [UIView animateWithDuration:0.5 animations:^{ emptyMessage.alpha = 1; }];
        } else {
            emptyMessage.alpha = 0;
        }
        
        
        [tableView reloadData];
        
    } failure:nil];
    [operation start];
}

- (void)toggleNotifications:(id)sender {
    //[self setEditing:!self.editing animated:YES];
    NSLog(@"notification view");
    
    CFENotificationsTableTableViewController *viewController = [[CFENotificationsTableTableViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

-(void)refresh {
    [self getReports];
    [refreshControl endRefreshing];
}
@end
