//
//  MoreTableViewCell.h
//  CFE
//
//  Created by Ernesto Vargas on 10/18/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *iconImageView;
@end
