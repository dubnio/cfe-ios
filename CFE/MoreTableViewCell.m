//
//  MoreTableViewCell.m
//  CFE
//
//  Created by Ernesto Vargas on 10/18/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "MoreTableViewCell.h"

@implementation MoreTableViewCell

@synthesize titleLabel = _titleLabel;
@synthesize iconImageView = _iconImageView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
        self.backgroundColor = [UIColor clearColor];
        
        // Icon
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13, 24.5, 24.5)];
        self.contentMode = UIViewContentModeLeft;
        [self addSubview:self.iconImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, 300, 30)];
        self.titleLabel.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
        self.titleLabel.font = [UIFont fontWithName:@"Whitney-Medium" size:16];
        [self addSubview:self.titleLabel];
        
    }
    return self;
}
@end
