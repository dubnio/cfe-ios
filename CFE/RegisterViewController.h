//
//  RegisterViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 11/3/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AppDelegate.h"
#import <RestKit/RestKit.h>
#import "User.h"
#import "UserToken.h"
#import "SVProgressHUD.h"

@interface RegisterViewController : UIViewController

@property (nonatomic, strong) UITextField *username;
@property (nonatomic, strong) UITextField *email;
@property (nonatomic, strong) UITextField *password;


@end
