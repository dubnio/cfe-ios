//
//  CreateNewReportViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 11/12/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import "Report.h"

@interface CreateNewReportViewController : UIViewController <CLLocationManagerDelegate>
    @property (nonatomic, copy) NSString *reportTypeId;
    @property (nonatomic, copy) NSString *contractId;
    @property (nonatomic, strong) UITextField *message;
@end
