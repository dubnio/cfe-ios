//
//  ReportListTableViewCell.h
//  CFE
//
//  Created by Ernesto Vargas on 10/15/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportListTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *statusTypeLabel;
@property (nonatomic, strong) UILabel *addressLabel;

@end
