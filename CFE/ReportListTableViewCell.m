//
//  ReportListTableViewCell.m
//  CFE
//
//  Created by Ernesto Vargas on 10/15/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "ReportListTableViewCell.h"

@implementation ReportListTableViewCell
@synthesize titleLabel = _titleLabel;
@synthesize statusTypeLabel = _statusTypeLabel;
@synthesize addressLabel = _addressLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize size = self.contentView.bounds.size;
    
    CGRect frame = self.textLabel.frame;
    frame.size.width = size.width - frame.origin.x - 10.0f - 7;
    self.textLabel.frame = frame;
}

#pragma mark - UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
        self.backgroundColor = [UIColor clearColor];
        float width = [[UIScreen mainScreen] bounds].size.width;
        
        // Background
        UIView *backview = [[UIView alloc]initWithFrame:CGRectMake(2, 2, width-4, 106)];
        [backview setBackgroundColor:[UIColor colorWithRed:0.988 green:0.929 blue:0.871 alpha:1]];
        backview.layer.cornerRadius = 2;
        backview.layer.borderColor = [[UIColor colorWithRed:0.953 green:0.898 blue:0.847 alpha:1] CGColor];
        backview.layer.borderWidth = .5;
        [self addSubview:backview];
        
        // Icon
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13.5, 20.5, 25)];
        imgView.image = [UIImage imageNamed:@"statusRepair"];
        imgView.contentMode = UIViewContentModeCenter;
        [self addSubview:imgView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, 300, 30)];
        self.titleLabel.textColor = [UIColor colorWithRed:0.247 green:0.227 blue:0.227 alpha:1];
        self.titleLabel.font = [UIFont fontWithName:@"Whitney-Semibold" size:17.5f];
        [self addSubview:self.titleLabel];
        
        // Status Label
        UILabel *statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 40, 300, 13)];
        statusLabel.textColor = [UIColor colorWithRed:0.396 green:0.365 blue:0.365 alpha:1];
        statusLabel.font = [UIFont fontWithName:@"Whitney-Medium" size:13.0f];
        statusLabel.text = @"STATUS:";
        [self addSubview:statusLabel];
        
        
        self.statusTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(95, 40, 300, 13)];
        self.statusTypeLabel.textColor = [UIColor colorWithRed:0.396 green:0.365 blue:0.365 alpha:1];
        self.statusTypeLabel.font = [UIFont fontWithName:@"Whitney-Medium" size:13.0f];
        [self addSubview:self.statusTypeLabel];
        
        self.addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 60, 300, 13)];
        self.addressLabel.textColor = [UIColor colorWithRed:0.714 green:0.655 blue:0.655 alpha:1];
        self.addressLabel.font = [UIFont fontWithName:@"Whitney-Medium" size:12];
        [self addSubview:self.addressLabel];

    }
    return self;
}

@end
