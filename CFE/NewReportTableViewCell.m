//
//  NewReportTableViewCell.m
//  CFE
//
//  Created by Ernesto Vargas on 10/22/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "NewReportTableViewCell.h"

@implementation NewReportTableViewCell

@synthesize titleLabel = _titleLabel;
@synthesize backview = _backview;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UIView

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize size = self.contentView.bounds.size;
    
    CGRect frame = self.textLabel.frame;
    frame.size.width = size.width - frame.origin.x - 10.0f - 7;
    self.textLabel.frame = frame;
}


#pragma mark - UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
        float width = [[UIScreen mainScreen] bounds].size.width;
        
        self.backgroundColor = [UIColor clearColor];
        
        // Background
        self.backview = [[UIView alloc]initWithFrame:CGRectMake(2, 2, width-4, 63)];
        //[backview setBackgroundColor:[UIColor colorWithRed:0.992 green:0.914 blue:0.914 alpha:1]];
        self.backview.layer.cornerRadius = 2;
        //backview.layer.borderColor = [[UIColor colorWithRed:0.957 green:0.882 blue:0.882 alpha:1] CGColor];
        self.backview.layer.borderWidth = .5;
        [self addSubview:self.backview];
        
        // Cell Label
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(49.5, 25, 230, 17.5)];
        //self.titleLabel.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
        self.titleLabel.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
        self.titleLabel.font = [UIFont fontWithName:@"Whitney-Medium" size:17.5];
        [self addSubview:self.titleLabel];
        
    }
    return self;
}

@end
