//
//  CFENotificationsTableTableViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
@import AudioToolbox;

@interface CFENotificationsTableTableViewController : UITableViewController

@end
