//
//  NewReportTableViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/12/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "NewReportTableViewController.h"

@interface NewReportTableViewController ()
    @property (assign, nonatomic) SystemSoundID            bloomSound;
    @property (assign, nonatomic) SystemSoundID            foldSound;
    @property (assign, nonatomic) SystemSoundID            selectedSound;
    @property (nonatomic, strong) NSMutableArray           *sections;
    @property (nonatomic, strong) NSMutableArray           *sectionTitles;
@end

@implementation NewReportTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"¿Cuál es su Situación?";
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButtonItem];
    
    [self.tableView registerClass:[NewReportTableViewCell class] forCellReuseIdentifier:@"cell"];
    
    //self.tableView.backgroundColor = [UIColor clearColor];
    //self.tableView.opaque = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self configureSounds];
    AudioServicesPlaySystemSound(self.bloomSound);
    
    // Close button
    UIImage* closeButton = [UIImage imageNamed:@"close-button"];
    CGRect frameimg = CGRectMake(0, 0, closeButton.size.width, closeButton.size.height);
    UIButton *bellButton = [[UIButton alloc] initWithFrame:frameimg];
    [bellButton setBackgroundImage:closeButton forState:UIControlStateNormal];
    [bellButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bellButton];
    
    self.sections = [[NSMutableArray alloc] init];
    NSArray *firstDataSection = @[
                                  @{
                                      @"label": @"No hay luz en colonia",
                                      @"color": [UIColor colorWithRed:0.984 green:0.859 blue:0.859 alpha:1],
                                      @"borderColor": [UIColor colorWithRed:0.949 green:0.827 blue:0.827 alpha:1],
                                      @"id": @"1"
                                      },
                                  @{
                                      @"label": @"No hay luz en mi casa",
                                      @"color": [UIColor colorWithRed:0.984 green:0.886 blue:0.867 alpha:1],
                                      @"borderColor": [UIColor colorWithRed:0.949 green:0.855 blue:0.835 alpha:1],
                                      @"id": @"2"
                                      },
                                  @{
                                      @"label": @"Variación de voltaje en colonia",
                                      @"color": [UIColor colorWithRed:0.984 green:0.902 blue:0.863 alpha:1],
                                      @"borderColor": [UIColor colorWithRed:0.949 green:0.871 blue:0.831 alpha:1],
                                      @"id": @"3"
                                      },
                                  @{
                                      @"label": @"Variación de voltaje en casa",
                                      @"color": [UIColor colorWithRed:0.988 green:0.929 blue:0.871 alpha:1],
                                      @"borderColor": [UIColor colorWithRed:0.953 green:0.898 blue:0.839 alpha:1],
                                      @"id": @"4"
                                      },
                                  @{
                                      @"label": @"CFEmático no funciona",
                                      @"color": [UIColor colorWithRed:0.992 green:0.949 blue:0.871 alpha:1],
                                      @"borderColor": [UIColor colorWithRed:0.957 green:0.914 blue:0.839 alpha:1],
                                      @"id": @"5"
                                      },
                                  ];
    NSDictionary *firstDataDict = @{@"fields" : firstDataSection};
    NSArray *secondDataSection = @[
                                   @{
                                       @"label": @"Tarifa excesiva en recibo",
                                       @"color": [UIColor colorWithRed:0.910 green:0.929 blue:0.925 alpha:1],
                                       @"borderColor": [UIColor colorWithRed:0.875 green:0.898 blue:0.894 alpha:1],
                                       @"id": @"6"
                                       },
                                   @{
                                       @"label": @"Extorsión o corrupción",
                                       @"color": [UIColor colorWithRed:0.933 green:0.953 blue:0.949 alpha:1],
                                       @"borderColor": [UIColor colorWithRed:0.902 green:0.918 blue:0.914 alpha:1],
                                       @"id": @"7"
                                       },
                                   @{
                                       @"label": @"Mal servicio en sucursal",
                                       @"color": [UIColor colorWithRed:0.957 green:0.969 blue:0.969 alpha:1],
                                       @"borderColor": [UIColor colorWithRed:0.922 green:0.933 blue:0.933 alpha:1],
                                       @"id": @"8"
                                       },
                                   ];
    NSDictionary *secondDataDict = @{@"fields" : secondDataSection};
    
    [self.sections addObjectsFromArray:@[firstDataDict, secondDataDict]];
    
    self.sectionTitles = [[NSMutableArray alloc] initWithObjects:@"FALLAS DE SERVICIO", @"INCONFORMIDADES",nil];
}

- (void)configureSounds
{
    // Configure bloom sound
    //
    NSString *bloomSoundPath = [[NSBundle mainBundle]pathForResource:@"bloom" ofType:@"caf"];
    NSURL *bloomSoundURL = [NSURL fileURLWithPath:bloomSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)bloomSoundURL, &_bloomSound);
    
    // Configure fold sound
    //
    NSString *foldSoundPath = [[NSBundle mainBundle]pathForResource:@"fold" ofType:@"caf"];
    NSURL *foldSoundURL = [NSURL fileURLWithPath:foldSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)foldSoundURL, &_foldSound);
    
    // Configure selected sound
    //
    NSString *selectedSoundPath = [[NSBundle mainBundle]pathForResource:@"selected" ofType:@"caf"];
    NSURL *selectedSoundURL = [NSURL fileURLWithPath:selectedSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)selectedSoundURL, &_selectedSound);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sections count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *dictionary = self.sections[section];
    NSArray *array = dictionary[@"fields"];
    return [array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 48;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Create Headerview
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    tableHeaderView.backgroundColor = [UIColor colorWithRed:0.980 green:0.980 blue:0.980 alpha:1];
    
    // Create Label
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 8, 320, 50)];
    label.text = [self.sectionTitles objectAtIndex:section];
    label.textColor = [UIColor colorWithRed:0.631 green:0.643 blue:0.651 alpha:1];
    label.font = [UIFont fontWithName:@"Whitney-Semibold" size:12];
    
    [tableHeaderView addSubview:label];
    return tableHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dictionary = self.sections[indexPath.section];
    NSArray *array = dictionary[@"fields"];
    NSDictionary *cellvalue = array[indexPath.row];
    
    NewReportTableViewCell *cell = (NewReportTableViewCell*)[theTableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.backview.backgroundColor = cellvalue[@"color"];
    cell.backview.layer.borderColor = (__bridge CGColorRef)(cellvalue[@"borderColor"]);
    cell.titleLabel.text = cellvalue[@"label"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictionary = self.sections[indexPath.section];
    NSArray *array = dictionary[@"fields"];
    NSDictionary *cellvalue = array[indexPath.row];
    NRContractsViewController *detailViewController = [[NRContractsViewController alloc] init];
    detailViewController.reportTypeId = cellvalue[@"id"];
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)cancel:(id)sender {
    AudioServicesPlaySystemSound(self.foldSound);
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
