//
//  MyContractsTableViewCell.m
//  CFE
//
//  Created by Ernesto Vargas on 11/11/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "MyContractsTableViewCell.h"

@implementation MyContractsTableViewCell

@synthesize titleLabel = _titleLabel;
@synthesize numberLabel = _numberLabel;
@synthesize iconImageView = _iconImageView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - UITableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        
        self.backgroundColor = [UIColor clearColor];
        
        // Icon
        self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 13, 24.5, 24.5)];
        self.contentMode = UIViewContentModeLeft;
        [self addSubview:self.iconImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, 300, 30)];
        self.titleLabel.textColor = [UIColor colorWithRed:0.247 green:0.227 blue:0.227 alpha:1];
        self.titleLabel.font = [UIFont fontWithName:@"Whitney-Semibold" size:17.5f];
        [self addSubview:self.titleLabel];
        
        self.numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 38, 300, 12)];
        self.numberLabel.textColor = [UIColor colorWithRed:0.247 green:0.227 blue:0.227 alpha:1];
        self.numberLabel.font = [UIFont fontWithName:@"Whitney-Medium" size:12.0f];
        [self addSubview:self.numberLabel];
    }
    return self;
}

@end
