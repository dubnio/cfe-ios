//
//  CFEMaticosViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CFEAnnotation.h"
#import "CFENotificationsTableTableViewController.h"
#import "AppDelegate.h"
#import "CFEmatico.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@class MKMapView;

@interface CFEMaticosViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) MKMapView *mapView;
@property(nonatomic, retain) CLLocationManager *locationManager;

@end
