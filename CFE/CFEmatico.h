//
//  CFEmatico.h
//  CFE
//
//  Created by Ernesto Vargas on 11/17/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CFEmatico : NSObject
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@end
