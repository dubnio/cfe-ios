//
//  CFELoginViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "CFELoginViewController.h"

@interface CFELoginViewController ()

@end

@implementation CFELoginViewController

@synthesize username;
@synthesize password;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Login";
    self.view.backgroundColor = [UIColor colorWithRed:0.980 green:0.980 blue:0.980 alpha:1];
    
    // Close Button
    UIImage* exImage = [UIImage imageNamed:@"close-button"];
    CGRect frameimg = CGRectMake(0, 0, exImage.size.width, exImage.size.height);
    UIButton *closeButton = [[UIButton alloc] initWithFrame:frameimg];
    [closeButton setBackgroundImage:exImage forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    //[someButton setShowsTouchWhenHighlighted:YES];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    //float height = [[UIScreen mainScreen] bounds].size.height;
    float width = [[UIScreen mainScreen] bounds].size.width;
    //float buttonHeight = 60;
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 93, width, 150)];
    backgroundView.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    backgroundView.layer.borderColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1].CGColor;
    backgroundView.layer.borderWidth = 0.5;
    [self.view addSubview:backgroundView];
    
    // Username
    username = [[UITextField alloc] initWithFrame:CGRectMake(13, 93, width-26, 75)];
    username.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    username.font = [UIFont fontWithName:@"Whitney-Medium" size:20];
    [username setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    //username.borderStyle = UITextBorderStyleRoundedRect;
    username.placeholder = @"Usuario";
    username.autocorrectionType = UITextAutocorrectionTypeNo;
    username.keyboardType = UIKeyboardTypeDefault;
    username.returnKeyType = UIReturnKeyNext;
    username.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [username becomeFirstResponder];
    username.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.view addSubview:username];
    
    // Password
    password = [[UITextField alloc] initWithFrame:CGRectMake(13, 168, width-26, 75)];
    password.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    password.secureTextEntry = YES;
    //password.borderStyle = UITextBorderStyleRoundedRect;
    [password setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    password.font = [UIFont fontWithName:@"Whitney-Medium" size:20];
    password.placeholder = @"Contraseña";
    password.keyboardType = UIKeyboardTypeDefault;
    password.returnKeyType = UIReturnKeyDone;
    password.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [password addTarget:self action:@selector(textFieldFinished:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.view addSubview:password];
    
    UIView *topBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 168, width, 0.5)];
    topBorder.backgroundColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1];
    [self.view addSubview:topBorder];
    
    // Forgot password label
    UILabel * forgotPassword = [[UILabel alloc] initWithFrame:CGRectMake(13, 255, 150, 13)];
    //forgotPassword.backgroundColor = [UIColor colorWithRed:0.957 green:0.573 blue:0.235 alpha:1];
    forgotPassword.text = @"Olvidaste tu contraseña?";
    forgotPassword.textColor = [UIColor colorWithRed:0.651 green:0.651 blue:0.651 alpha:1];
    forgotPassword.font = [UIFont fontWithName:@"Whitney-Medium" size:13];
    [self.view addSubview:forgotPassword];

    // Forgot password button
    UIButton *resetPasswordButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [resetPasswordButton  setTitle:@"Recuperar contraseña" forState:UIControlStateNormal];
    [resetPasswordButton addTarget:self action:@selector(resetPassword:) forControlEvents:UIControlEventTouchUpInside];
    [resetPasswordButton sizeToFit];
    resetPasswordButton.frame = CGRectMake(143, 255, 150, 13);
    resetPasswordButton.titleLabel.font  = [UIFont fontWithName:@"Whitney-Medium" size:13];
    [resetPasswordButton setTitleColor:[UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1] forState:UIControlStateNormal];

    [self.view addSubview:resetPasswordButton];
}

- (void)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)textFieldFinished:(id)sender
{
    [SVProgressHUD showWithStatus:@"Iniciando Sesión"];
    
    User *userToken = [User new];
    userToken.username = self.username.text;
    userToken.password = self.password.text;
    
    RKObjectManager *manager = [RKObjectManager sharedManager];
    
    [manager postObject:userToken path:@"tokenauth/" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        NSLog(@"======> tokenauth/: %@", [[result firstObject] token]);
        [SVProgressHUD showSuccessWithStatus:@"Éxito!"];
        
        NSString *token = [[result firstObject] token];
        [[RKObjectManager sharedManager].HTTPClient setDefaultHeader:@"Authorization" value:[NSString stringWithFormat:@" token %@", token]];
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:token forKey:@"token"];
        
        UIViewController *viewController = [[BaseViewController alloc] init];
        [self.parentViewController.view removeFromSuperview];
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        del.window.rootViewController = viewController;
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"<=====> Error: %@", [[error userInfo] objectForKey:@"NSLocalizedRecoverySuggestion"]);
        [SVProgressHUD showErrorWithStatus:@"Error al iniciar sesión"];
    }];
    
}

- (void)resetPassword:(id)sender {
    //[self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
