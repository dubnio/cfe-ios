//
//  ReportListTableViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 10/12/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportListTableViewCell.h"
#import "CFENotificationsTableTableViewController.h"
#import "AppDelegate.h"
#import "Report.h"

@interface ReportListTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
