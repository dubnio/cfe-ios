//
//  AppDelegate.m
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "AppDelegate.h"
#import "CFELoginViewController.h"
#import "BaseViewController.h"
#import <RestKit/RestKit.h>
#import "Contract.h"
#import "Report.h"
#import "CFEmatico.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //
    // Styles
    //
    application.statusBarStyle = UIStatusBarStyleLightContent;
    
    UIScreen *screen = [UIScreen mainScreen];
    CGRect frame = [screen bounds];
    self.window = [[UIWindow alloc] initWithFrame:frame];
    self.window.backgroundColor = [UIColor whiteColor];
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:0.118 green:0.502 blue:0.243 alpha:1]];
    [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1]];
    
    UITabBarItem *tabBarItem = [UITabBarItem appearance];
    [tabBarItem setTitleTextAttributes:@{
                                         NSForegroundColorAttributeName: [UIColor colorWithRed:0.118 green:0.502 blue:0.247 alpha:1],
                                         NSFontAttributeName: [UIFont fontWithName:@"Whitney-Medium" size:10.0f]
                                         } forState:UIControlStateSelected];
    [tabBarItem setTitleTextAttributes:@{
                                         NSForegroundColorAttributeName: [UIColor colorWithRed:0.545 green:0.573 blue:0.600 alpha:1],
                                         NSFontAttributeName: [UIFont fontWithName:@"Whitney-Medium" size:10.0f]
                                         } forState:UIControlStateNormal];
    
    UINavigationBar *navigationBar = [UINavigationBar appearance];
    navigationBar.barTintColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    navigationBar.tintColor = [UIColor colorWithRed:0.118 green:0.498 blue:0.239 alpha:1];
    navigationBar.titleTextAttributes = @{
                                          NSForegroundColorAttributeName: [UIColor colorWithRed:0.118 green:0.498 blue:0.239 alpha:1],
                                          NSFontAttributeName: [UIFont fontWithName:@"Whitney-Semibold" size:18.0f]
                                          };
    UIBarButtonItem *barButtonItem = [UIBarButtonItem appearance];
    [barButtonItem setTitleTextAttributes:@{
                                            NSFontAttributeName: [UIFont fontWithName:@"Whitney-Semibold" size:16.0f]
                                            } forState:UIControlStateNormal];
    
    //
    // Restkit configurations
    //
    
    //let AFNetworking manage the activity indicator
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace)
    
    NSURL *baseURL = [NSURL URLWithString:@"http://cfe.herokuapp.com/api/"];
    AFHTTPClient* client = [[AFHTTPClient alloc] initWithBaseURL:baseURL];
    [client setDefaultHeader:@"Accept" value:RKMIMETypeJSON];
    RKObjectManager *objectManager = [[RKObjectManager alloc] initWithHTTPClient:client];
    
    
    RKObjectMapping *userResponseMapping = [RKObjectMapping mappingForClass:[User class]];
    [userResponseMapping addAttributeMappingsFromDictionary:@{@"token": @"token",  @"email": @"email", @"username": @"username"}];
    
    //NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx
    RKResponseDescriptor *userDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:userResponseMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:nil];
    
    RKObjectMapping *userRequestMapping = [RKObjectMapping requestMapping]; // objectClass == NSMutableDictionary
    [userRequestMapping addAttributeMappingsFromDictionary:@{ @"username": @"username", @"password": @"password",  @"email": @"email"}];
    
    RKRequestDescriptor *userRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:userRequestMapping objectClass:[User class] rootKeyPath:nil method:RKRequestMethodAny];
    

    [objectManager addRequestDescriptor:userRequestDescriptor];
    [objectManager addResponseDescriptor:userDescriptor];
    
    
    //
    // Contract Model
    //
    RKObjectMapping *contractResponseMapping = [RKObjectMapping mappingForClass:[Contract class]];
    [contractResponseMapping addAttributeMappingsFromDictionary:@{@"name": @"name", @"user": @"user",  @"number": @"number"}];
    
    RKResponseDescriptor *contractDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:contractResponseMapping method:RKRequestMethodAny pathPattern:nil keyPath:@"" statusCodes:nil];
    
    RKObjectMapping *contractRequestMapping = [RKObjectMapping requestMapping];
    [contractRequestMapping addAttributeMappingsFromDictionary:@{@"name": @"name", @"user": @"user",  @"number": @"number"}];
    
     RKRequestDescriptor *contractRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:contractRequestMapping objectClass:[Contract class] rootKeyPath:nil method:RKRequestMethodAny];
    
    [objectManager addRequestDescriptor:contractRequestDescriptor];
    [objectManager addResponseDescriptor:contractDescriptor];
    
    
    //
    // Report Model Map
    //
    RKObjectMapping *reportResponseMapping = [RKObjectMapping mappingForClass:[Report class]];
    [reportResponseMapping addAttributeMappingsFromDictionary:@{
                                                                @"contract": @"contract",
                                                                @"category": @"category",
                                                                @"comments": @"comments",
                                                                @"status": @"status",
                                                                @"latitude": @"latitude",
                                                                @"longitude": @"longitude",
                                                                }];
    
    RKResponseDescriptor *reportDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:reportResponseMapping method:RKRequestMethodAny pathPattern:nil keyPath:@"" statusCodes:nil];
    
    RKObjectMapping *reportRequestMapping = [RKObjectMapping requestMapping];
    [reportRequestMapping addAttributeMappingsFromDictionary:@{
                                                               @"contract": @"contract",
                                                               @"category": @"category",
                                                               @"comments": @"comments",
                                                               @"status": @"status",
                                                               @"latitude": @"latitude",
                                                               @"longitude": @"longitude",
                                                               }];
    
    RKRequestDescriptor *reportRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:reportRequestMapping objectClass:[Report class] rootKeyPath:nil method:RKRequestMethodAny];
    
    [objectManager addRequestDescriptor:reportRequestDescriptor];
    [objectManager addResponseDescriptor:reportDescriptor];
    
    
    //
    // CFEmatico Map model
    //
    RKObjectMapping *cfematicoResponseMapping = [RKObjectMapping mappingForClass:[CFEmatico class]];
    [cfematicoResponseMapping addAttributeMappingsFromDictionary:@{
                                                                @"name": @"name",
                                                                @"address": @"address",
                                                                @"latitude": @"latitude",
                                                                @"longitude": @"longitude",
                                                                }];
    RKResponseDescriptor *cfematicoDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:cfematicoResponseMapping method:RKRequestMethodAny pathPattern:nil keyPath:@"" statusCodes:nil];
    
    RKObjectMapping *cfematicoRequestMapping = [RKObjectMapping requestMapping];
    [cfematicoRequestMapping addAttributeMappingsFromDictionary:@{
                                                               @"contract": @"contract",
                                                               @"category": @"category",
                                                               @"comments": @"comments",
                                                               @"status": @"status",
                                                               @"latitude": @"latitude",
                                                               @"longitude": @"longitude",
                                                               }];
    RKRequestDescriptor *cfematicoRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:cfematicoRequestMapping objectClass:[CFEmatico class] rootKeyPath:nil method:RKRequestMethodAny];
    [objectManager addRequestDescriptor:cfematicoRequestDescriptor];
    [objectManager addResponseDescriptor:cfematicoDescriptor];
    
    
    //
    // User Defaults
    //
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    
    if([userDefaults objectForKey:@"token"] == nil) {
        UIViewController *startViewController = [[StartViewController alloc] init];
        self.window.rootViewController = startViewController;
        
    } else {
        NSLog(@"Using token: %@", [userDefaults objectForKey:@"token"]);
        [[RKObjectManager sharedManager].HTTPClient setDefaultHeader:@"Authorization" value:[NSString stringWithFormat:@" token %@", [userDefaults objectForKey:@"token"]]];
        UIViewController *viewController = [[BaseViewController alloc] init];
        self.window.rootViewController = viewController;
    }
    
    [self.window makeKeyAndVisible];
    
    return YES;
}


@end

//Whitney-Medium
//Whitney-BoldSC
//Whitney-SemiboldSC
//Whitney-Semibold