//
//  AddContractViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 11/10/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "AddContractViewController.h"

@interface AddContractViewController ()

@end

@implementation AddContractViewController

@synthesize name;
@synthesize user;
@synthesize number;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Agregar contrato";
    self.view.backgroundColor = [UIColor colorWithRed:0.980 green:0.980 blue:0.980 alpha:1];
    
    // Close Button
    UIImage* exImage = [UIImage imageNamed:@"close-button"];
    CGRect frameimg = CGRectMake(0, 0, exImage.size.width, exImage.size.height);
    UIButton *closeButton = [[UIButton alloc] initWithFrame:frameimg];
    [closeButton setBackgroundImage:exImage forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    //[someButton setShowsTouchWhenHighlighted:YES];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    //float height = [[UIScreen mainScreen] bounds].size.height;
    float width = [[UIScreen mainScreen] bounds].size.width;
    //float buttonHeight = 60;
    
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 93, width, 150)];
    backgroundView.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    backgroundView.layer.borderColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1].CGColor;
    backgroundView.layer.borderWidth = 0.5;
    [self.view addSubview:backgroundView];
    
    // Name
    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 93, width-26, 75)];
    name.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    name.font = [UIFont fontWithName:@"Whitney-Medium" size:20];
    [name setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    //username.borderStyle = UITextBorderStyleRoundedRect;
    name.placeholder = @"Nombre del servicio";
    name.autocorrectionType = UITextAutocorrectionTypeDefault;
    name.keyboardType = UIKeyboardTypeDefault;
    name.returnKeyType = UIReturnKeyNext;
    name.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [name becomeFirstResponder];
    name.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.view addSubview:name];
    
    // Service Number
    number = [[UITextField alloc] initWithFrame:CGRectMake(13, 168, width-26, 75)];
    number.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    [number setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    number.font = [UIFont fontWithName:@"Whitney-Medium" size:20];
    number.placeholder = @"Número de servicio";
    number.keyboardType = UIKeyboardTypeDefault;
    number.returnKeyType = UIReturnKeyDone;
    number.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [number addTarget:self action:@selector(textFieldFinished:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.view addSubview:number];
    
    UIView *topBorder = [[UIView alloc] initWithFrame:CGRectMake(0, 168, width, 0.5)];
    topBorder.backgroundColor = [UIColor colorWithRed:0.878 green:0.878 blue:0.878 alpha:1];
    [self.view addSubview:topBorder];
    
}


- (void)textFieldFinished:(id)sender
{
    [SVProgressHUD showWithStatus:@"Agregando contrato"];
    Contract *contract = [Contract new];
    contract.name = self.name.text;
    contract.user = self.user.text;
    contract.number = self.number.text;
    
    RKObjectManager *manager = [RKObjectManager sharedManager];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [[RKObjectManager sharedManager].HTTPClient setDefaultHeader:@"Authorization" value:[NSString stringWithFormat:@" token %@", [userDefaults objectForKey:@"token"]]];
    
    [manager postObject:contract path:@"contracts/" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        NSLog(@"======> Created Contract: %@", [result firstObject]);
        [SVProgressHUD showSuccessWithStatus:@"Éxito!"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadMyContractsTable" object:nil];

        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"<=====> Error: %@", [[error userInfo] objectForKey:@"NSLocalizedRecoverySuggestion"]);
        [SVProgressHUD showErrorWithStatus:@"Error al crear nuevo contrato"];
    }];
}
- (void)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
