//
//  CreateNewReportViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 11/12/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "CreateNewReportViewController.h"

@interface CreateNewReportViewController ()

@end

@implementation CreateNewReportViewController {
    CLLocationManager *locationManager;
    float latitude;
    float longitude;
}

@synthesize message;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Mensaje";
    self.view.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButtonItem];
    
    //float height = [[UIScreen mainScreen] bounds].size.height;
    float width = [[UIScreen mainScreen] bounds].size.width;
    
    // Close button
    UIImage* closeButton = [UIImage imageNamed:@"close-button"];
    CGRect frameimg = CGRectMake(0, 0, closeButton.size.width, closeButton.size.height);
    UIButton *bellButton = [[UIButton alloc] initWithFrame:frameimg];
    [bellButton setBackgroundImage:closeButton forState:UIControlStateNormal];
    [bellButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bellButton];
    
    // Message
    message = [[UITextField alloc] initWithFrame:CGRectMake(13, 93, width-26, 75)];
    message.textColor = [UIColor colorWithRed:0.349 green:0.349 blue:0.349 alpha:1];
    message.font = [UIFont fontWithName:@"Whitney-Medium" size:20];
    [message setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    //username.borderStyle = UITextBorderStyleRoundedRect;
    message.placeholder = @"Cual es la situacion?";
    message.autocorrectionType = UITextAutocorrectionTypeDefault;
    message.keyboardType = UIKeyboardTypeDefault;
    message.returnKeyType = UIReturnKeyGo;
    message.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [message becomeFirstResponder];
    message.clearButtonMode = UITextFieldViewModeWhileEditing;
    [message addTarget:self action:@selector(textFieldFinished:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.view addSubview:message];
    
    // Location
    locationManager = [[CLLocationManager alloc]init]; // initializing locationManager
    locationManager.delegate = self; // we set the delegate of locationManager to self.
    locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
    [locationManager startUpdatingLocation];  //requesting location updates
}

- (void)textFieldFinished:(id)sender
{
    [SVProgressHUD showWithStatus:@"Creando Reporte"];
    
    Report *report = [Report new];
    report.contract = self.contractId;
    report.category = self.reportTypeId;
    report.status = @"1";
    report.latitude = [NSString stringWithFormat:@"%.8f", latitude];
    report.longitude = [NSString stringWithFormat:@"%.8f", longitude];
    report.comments = message.text;
    
    RKObjectManager *manager = [RKObjectManager sharedManager];
    
    [manager postObject:report path:@"reports/" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        
        NSLog(@"======> Created New Report: %@", [result firstObject]);
        [SVProgressHUD showSuccessWithStatus:@"Éxito!"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadReportListTable" object:nil];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"<=====> Error: %@", [[error userInfo] objectForKey:@"NSLocalizedRecoverySuggestion"]);
        [SVProgressHUD showErrorWithStatus:@"Error creating a new reporte"];
    }];
    
}

- (void)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *crnLoc = [locations lastObject];
    latitude = crnLoc.coordinate.latitude;
    longitude = crnLoc.coordinate.longitude;
}
@end
