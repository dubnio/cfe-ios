//
//  CFEHomeViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "CFEHomeViewController.h"

@interface CFEHomeViewController ()

@property (nonatomic, strong) iCarousel *carousel;
@property (nonatomic, strong) NSArray *pictures;

@end

@implementation CFEHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _pictures = @[@"recibo", @"medidores", @"recibo", @"contacto", @"switch", @"transformador"];
    
    //create carousel
    _carousel = [[iCarousel alloc] initWithFrame:self.view.bounds];
    _carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _carousel.type = iCarouselTypeRotary;
    _carousel.delegate = self;
    _carousel.dataSource = self;
    
    //_pictures = @[@"recibo", @"diablitos", @"medidores"];
    
    //add carousel to view
    [self.view addSubview:_carousel];
}

- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.navigationItem.title = @"Inicio";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)toggleNotifications:(id)sender {
    //[self setEditing:!self.editing animated:YES];
    
    CFENotificationsTableTableViewController *viewController = [[CFENotificationsTableTableViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel
{
    return [_pictures count];
}

- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        ((UIImageView *)view).image = [UIImage imageNamed:[_pictures objectAtIndex:index]];
        view.contentMode = UIViewContentModeCenter;
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        //label.textAlignment = UITextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    //label.text = @"hola";
    
    return view;
}



@end
