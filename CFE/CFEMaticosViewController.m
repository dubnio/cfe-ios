//
//  CFEMaticosViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "CFEMaticosViewController.h"

@interface CFEMaticosViewController ()

@end

@implementation CFEMaticosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestAlwaysAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    //self.navigationController.navigationBar.translucent = NO;
    self.mapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
    self.mapView.delegate = self;
    
    self.mapView.showsUserLocation = YES;
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.mapView];
    
    CLLocationCoordinate2D location;
    location.latitude = 19.414870;
    location.longitude = -99.172125;
    
    MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
    myAnnotation.coordinate = location;
    

    
    //[self.mapView addAnnotation:myAnnotation];
    
    MKCoordinateRegion region;
    region.center = location;
    region.span.latitudeDelta = 0.005;
    region.span.longitudeDelta = 0.005;
    [self.mapView setRegion:region animated:YES];
    
    // Add button for controlling user location tracking
    MKUserTrackingBarButtonItem *trackingButton =
    [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    self.navigationItem.leftBarButtonItem = trackingButton;
    
    // Notification button
    UIImage* bellImage = [UIImage imageNamed:@"bell"];
    CGRect frameimg = CGRectMake(0, 0, bellImage.size.width, bellImage.size.height);
    UIButton *bellButton = [[UIButton alloc] initWithFrame:frameimg];
    [bellButton setBackgroundImage:bellImage forState:UIControlStateNormal];
    [bellButton addTarget:self action:@selector(toggleNotifications:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bellButton];
    
    //
    [self getReports];
}

- (void)viewWillAppear:(BOOL)animated {
    self.title = @"CFEmático";
}

-(MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    // Don't create annotation views for the user location annotation
    if ([annotation isKindOfClass:[CFEAnnotation class]]) {
        MKPinAnnotationView *MyPin=[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"current"];
        UIButton *advertButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //[advertButton addTarget:self action:@selector(button:) forControlEvents:UIControlEventTouchUpInside];
        MyPin.rightCalloutAccessoryView = advertButton;
        //MyPin.animatesDrop=TRUE;
        MyPin.canShowCallout = YES;
        MyPin.highlighted = NO;
        MyPin.image = [UIImage imageNamed:@"cfematico"];
        return MyPin;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    CGRect visibleRect = [mapView annotationVisibleRect];
    for (MKAnnotationView *view in views)
    {
        CGRect endFrame = view.frame;
        endFrame.origin.y -= 15.0f;
        endFrame.origin.x += 8.0f;
        CGRect startFrame = endFrame;
        startFrame.origin.y = visibleRect.origin.y - 150;// - startFrame.size.height;
        view.frame = startFrame;
        
        [UIView beginAnimations:@"drop" context:NULL];
        [UIView setAnimationDuration:0.6];
        
        view.frame = endFrame;
        
        [UIView commitAnimations];
    }
}

- (void)getReports {
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[CFEmatico class]];
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"name": @"name",
                                                  @"address": @"address",
                                                  @"latitude": @"latitude",
                                                  @"longitude": @"longitude"
                                                  }];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPOST pathPattern:nil keyPath:nil statusCodes:nil];
    NSURL *url = [NSURL URLWithString:@"http://cfe.herokuapp.com/api/cac/"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [mutableRequest addValue:[NSString stringWithFormat:@" token %@", [userDefaults objectForKey:@"token"]] forHTTPHeaderField:@"Authorization"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSLog(@"CFEmaticos: %@", [result array]);
        
        for (CFEmatico *cfematico in [result array]) {
            
            CLLocationCoordinate2D location;
            location.latitude = [cfematico.latitude floatValue];;
            location.longitude = [cfematico.longitude floatValue];;
            
            CFEAnnotation *annotation =  [[CFEAnnotation alloc]
                                          initWithCoordinates:location
                                          title:cfematico.name
                                          subTitle:cfematico.address
                                          image:@"home"];
            
            [self.mapView addAnnotation:annotation];
        }
    } failure:nil];
    [operation start];
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    view.image = [UIImage imageNamed:@"cfematico-selected"];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
     view.image = [UIImage imageNamed:@"cfematico"];
}

#pragma mark - Actions

- (void)toggleNotifications:(id)sender {
    //[self setEditing:!self.editing animated:YES];
    CFENotificationsTableTableViewController *viewController = [[CFENotificationsTableTableViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}
@end
