//
//  MoreTableViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/13/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "MoreTableViewController.h"

@interface MoreTableViewController ()
    @property (nonatomic, strong) NSMutableArray           *sections;
@end

@implementation MoreTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Más";
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButtonItem];
    
    [self.tableView registerClass:[MoreTableViewCell class] forCellReuseIdentifier:@"cell"];

    self.sections = [[NSMutableArray alloc] init];
    NSArray *firstDataSection = @[
                                  @{
                                      @"id": @"editProfile",
                                      @"icon": @"editProfile",
                                      @"label": @"Modificar mi Perfil",
                                      },
                                  @{
                                      @"id": @"paymentHistory",
                                      @"icon": @"paymentHistory",
                                      @"label": @"Ver Historial de Pagos",
                                      },
                                  @{
                                      @"id": @"myReceipts",
                                      @"icon": @"myReceipts",
                                      @"label": @"Mis Recibos",
                                      },
                                  @{
                                      @"id": @"myContracts",
                                      @"icon": @"myReceipts",
                                      @"label": @"Mis Contratos",
                                      }
                                  ];
    NSDictionary *firstDataDict = @{@"fields" : firstDataSection};
    NSArray *secondDataSection = @[
                                  @{
                                      @"id": @"logout",
                                      @"icon": @"logout",
                                      @"label": @"Cerrar Sesión",
                                      },
                                  ];
    NSDictionary *secondDataDict = @{@"fields" : secondDataSection};
    NSArray *thirdDataSection = @[
                                   @{
                                       @"id": @"deleteAccount",
                                       @"icon": @"deleteAccount",
                                       @"label": @"Eliminar mi Cuenta",
                                       },
                                   ];
    NSDictionary *thirdDataDict = @{@"fields" : thirdDataSection};
    
    [self.sections addObjectsFromArray:@[firstDataDict, secondDataDict, thirdDataDict]];
    
    // Notification button
    UIImage* bellImage = [UIImage imageNamed:@"bell"];
    CGRect frameimg = CGRectMake(0, 0, bellImage.size.width, bellImage.size.height);
    UIButton *bellButton = [[UIButton alloc] initWithFrame:frameimg];
    [bellButton setBackgroundImage:bellImage forState:UIControlStateNormal];
    [bellButton addTarget:self action:@selector(toggleNotifications:) forControlEvents:UIControlEventTouchUpInside];
    //[someButton setShowsTouchWhenHighlighted:YES];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:bellButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *dictionary = self.sections[section];
    NSArray *array = dictionary[@"fields"];
    return [array count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 31.5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // Create Headerview
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    tableHeaderView.backgroundColor = [UIColor colorWithRed:0.980 green:0.980 blue:0.980 alpha:1];
    return tableHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dictionary = self.sections[indexPath.section];
    NSArray *array = dictionary[@"fields"];
    NSDictionary *cellvalue = array[indexPath.row];
    
    MoreTableViewCell *cell = (MoreTableViewCell*)[theTableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.iconImageView.image = [UIImage imageNamed:cellvalue[@"icon"]];
    cell.titleLabel.text = cellvalue[@"label"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictionary = self.sections[indexPath.section];
    NSArray *array = dictionary[@"fields"];
    NSDictionary *cellvalue = array[indexPath.row];
    
    if ([cellvalue[@"id"] isEqualToString:@"editProfile"]){
        ProfileViewController *viewController = [[ProfileViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
       // [self.navigationController pushViewController:viewController animated:YES];
    }
    if ([cellvalue[@"id"] isEqualToString:@"paymentHistory"]){
        PaymentHistoryViewController *viewController = [[PaymentHistoryViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    if ([cellvalue[@"id"] isEqualToString:@"myReceipts"]){
        MyReceiptsViewController *viewController = [[MyReceiptsViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    }
    
    if ([cellvalue[@"id"] isEqualToString:@"myContracts"]){
        MyContractsViewController *viewController = [[MyContractsViewController alloc] init];
        [self.navigationController pushViewController:viewController animated:YES];
    }

    if ([cellvalue[@"id"] isEqualToString:@"logout"]){
        UIAlertView* finalCheck = [[UIAlertView alloc]
                                   initWithTitle:@"Confirmación"
                                   message:@"Seguro que quiere Cerrar Sesión"
                                   delegate:self
                                   cancelButtonTitle:@"Cancelar"
                                   otherButtonTitles:@"Si",nil];
        
        [finalCheck show];
    }

    if ([cellvalue[@"id"] isEqualToString:@"deleteAccount"]){
        UIAlertView* finalCheck = [[UIAlertView alloc]
                                   initWithTitle:@"Confirmación"
                                   message:@"Seguro que quiere eliminar su cuenta"
                                   delegate:self
                                   cancelButtonTitle:@"Cancelar"
                                   otherButtonTitles:@"Si",nil];
        
        [finalCheck show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1) {
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:nil forKey:@"token"];
        [userDefaults setObject:nil forKey:@"showIntro"];
        
        UIViewController *viewController = [[StartViewController alloc] init];
        [self.parentViewController.view removeFromSuperview];
        AppDelegate *del = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        del.window.rootViewController = viewController;
    }
}

#pragma mark - Actions

- (void)toggleNotifications:(id)sender {
    //[self setEditing:!self.editing animated:YES];
    NSLog(@"notification view");
    
    CFENotificationsTableTableViewController *viewController = [[CFENotificationsTableTableViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

@end
