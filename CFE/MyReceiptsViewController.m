//
//  MyReceiptsViewController.m
//  CFE
//
//  Created by Ernesto Vargas on 10/18/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "MyReceiptsViewController.h"

@interface MyReceiptsViewController ()

@end

@implementation MyReceiptsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Mis Recibos";
    self.view.backgroundColor = [UIColor colorWithRed:1.000 green:1.000 blue:1.000 alpha:1];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [self.navigationItem setBackBarButtonItem:backButtonItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
