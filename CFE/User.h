//
//  User.h
//  CFE
//
//  Created by Ernesto Vargas on 11/3/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *token;

@end
