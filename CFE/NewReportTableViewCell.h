//
//  NewReportTableViewCell.h
//  CFE
//
//  Created by Ernesto Vargas on 10/22/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewReportTableViewCell : UITableViewCell
    @property (nonatomic, strong) UILabel *titleLabel;
    @property (nonatomic, strong) UIView *backview;
@end
