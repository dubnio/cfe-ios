//
//  BaseViewController.h
//  CFE
//
//  Created by Ernesto Vargas on 10/7/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAIntroView.h"

@interface BaseViewController : UITabBarController <EAIntroDelegate>

@end
