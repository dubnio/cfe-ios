//
//  CFEAnnotation.m
//  CFE
//
//  Created by Ernesto Vargas on 10/9/14.
//  Copyright (c) 2014 Ernesto Vargas. All rights reserved.
//

#import "CFEAnnotation.h"

@implementation CFEAnnotation

- (instancetype) initWithCoordinates:(CLLocationCoordinate2D)paramCoordinates title:(NSString*)paramTitle subTitle:(NSString*)paramSubTitle image:(NSString*)image{
    
    self = [super init];
    
    if (self != nil){
        _coordinate = paramCoordinates;
        _title = paramTitle;
        _subtitle = paramSubTitle;
        _pinColor = MKPinAnnotationColorGreen;
        _image = image;
    }
    
    return self;
    
}

@end
